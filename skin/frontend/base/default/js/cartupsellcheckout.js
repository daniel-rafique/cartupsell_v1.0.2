var CartUpsellCheckout = Class.create(Checkout, {
  initialize: function($super,accordion, urls){
    $super(accordion, urls);
    this.steps = ['login' ,'billing', 'shipping' ,'cartupsell', 'shipping_method', 'payment', 'review'];
  }
});

var CartupsellMethod = Class.create();
CartupsellMethod.prototype = {
    initialize: function(form, saveUrl){
        this.form = form;
        if ($(this.form)) {
            $(this.form).observe('submit', function(event){this.submit();Event.stop(event);}.bind(this));
        }
///this.action=        $('product_addtocart').readAttribute('action')
        this.saveUrl = saveUrl;
        this.onSave = this.nextStep.bindAsEventListener(this);
        this.onSave0 = this.next0Step.bindAsEventListener(this);

        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
    },

    submit: function(t){
    var action=$('product_addtocart_form').readAttribute('action');
    var form=$('product_addtocart_form');
        if (checkout.loadWaiting!=false) return;

///        checkout.setLoadWaiting('cartupsell');
        var request = new Ajax.Request(
            action,
            {
                method:'post',
                onComplete: this.onComplete,
                onSuccess: this.onSave0,
                onFailure: checkout.ajaxFailure.bind(checkout),
                parameters: Form.serialize(form) + "&isAjax=1&bCartUpselladdProductToCard="
            }
        );

    },

    resetLoadWaiting: function(transport){
        checkout.setLoadWaiting(false);
    },
    save2:function(){
    	
    	var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: "bCartUpselladdProductToCard=-1&isAjax=1"
                });
    	return false;
    	
    },
    next0Step: function(transport){
    	if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
          if(response.status=='SUCCESS'){
        	  //alert('success');
          }else{
        	  alert('Error in adding item to cart');
            	window.location.reload();
           return;     
          }
            
            }
            catch (e) {
            	alert('Error in adding item to cart');
            	window.location.reload();
                //response = {};
            return;
            }

    	
    	}else{
        	
        	alert('Error in adding item to cart');
        	window.location.reload();
        	return;
        }
    	
    	
    	var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: "bCartUpselladdProductToCard=-1&isAjax=1"
                }
            );

    	
    },

    nextStep: function(transport){
	
    	if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }
    	

        if (response.error) {
            alert(response.message);
            return false;
        }
        

        if (response.update_section) {

        	if(response.update_section.name=='cartupsell'){

        	$j=jQuery.noConflict();
        	
        	
        	
        	
        	
            
        	
        	var dom=$j(response.update_section.html);
        	dom.filter('script').each(function(){
        		
        	       if(this.src) {
               	   var script = document.createElement('script'), i, attrName, attrValue, attrs = this.attributes;
                       for(i = 0; i < attrs.length; i++) {
                           attrName = attrs[i].name;
                           attrValue = attrs[i].value;
                           script[attrName] = attrValue;
                       }
                       document.body.appendChild(script);
                   } else {
                       try{
                	   $j.globalEval(this.text || this.textContent || this.innerHTML || '');
                       }catch(e){
                    	   console.log("Error");
                    	   console.log(e);
                       }
                   }
               

        		
        		
        	})
document.getElementById('checkout-'+response.update_section.name+'-load').innerHTML=response.update_section.html;

            	
            	
            }
            else{
            	$('checkout-'+response.update_section.name+'-load').update(response.update_section.html);
            }
        }


        if (response.goto_section) {
            checkout.gotoSection(response.goto_section);
            checkout.reloadProgressBlock();
            return;
        }

        //checkout.checkout.setShipping();
    }
}