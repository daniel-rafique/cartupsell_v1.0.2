<?php
require_once 'Mage/Checkout/controllers/CartController.php';

 
 class Thaneuk_Cartupsell_CartController extends Mage_Checkout_CartController{
	
 	public function addAction()
 	{
 		$cart   = $this->_getCart();
 		$params = $this->getRequest()->getParams();
 		if($params['isAjax'] == 1){
 			
 			$result=array('ok'=>1);
 			
 			
 			$response = array();
 			try {
 				if (isset($params['qty'])) {
 					$filter = new Zend_Filter_LocalizedToNormalized(
 							array('locale' => Mage::app()->getLocale()->getLocaleCode())
 					);
 					$params['qty'] = $filter->filter($params['qty']);
 				}
 			
 				$product = $this->_initProduct();
 					
 			
 				$related = $this->getRequest()->getParam('related_product');
 			
 				/**
 				 * Check product availability
 				*/
 				if (!$product) {
 					$response['status'] = 'ERROR';
 					$response['message'] = $this->__('Unable to find Product ID');
 				}
 			
 				$cart->addProduct($product, $params);
 				if (!empty($related)) {
 					$cart->addProductsByIds(explode(',', $related));
 				}
 			
 			
 				$cart->save();
 			$p=array();
 			
 				$this->_getSession()->setCartWasUpdated(true);
 				if($product->getTypeId()==Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
 				$pritem=	$cart->getQuote()->getItemByProduct($product);
 					$tid=$pritem->getId();
 					
 					
 					foreach ($cart->getItems() as $item) {
 						// we can wrap our request in this IF statement
 						if ($item->getParentItemId()&&$item->getParentItemId()==$tid) {
 					
 							$p[]=$item;
 							
 							
 							
 							
 						}//itemparent
 					}//cartitems

 					$cookie = Mage::getSingleton('core/cookie');
 					
 					$cookie->set('bundleadded',$product->getId());
 					
 					
 				}//tb
 				else{
 					$cookie = Mage::getSingleton('core/cookie');
 					
 					$cookie->set('relateadded',$product->getId());
 					
 				}
 				
 				foreach($p as $pitem){
 					$pid=$pitem->getProductId();
 					$itid=$pitem->getId();
 					foreach($cart->getItems() as $item){
 						if(!$item->getParentItemId()){
 							if($pid==$item->getProductId()){
 								Mage::log("duplicate removed ".$item->getProuctId());
 								$cookie = Mage::getSingleton('core/cookie');
 									
 								$cookie->set('removeditem',$item->getProductId());
 									
 								$cart->removeItem($item->getId());
 									
 								
 							}
 						}
 						
 						
 						
 					}//citem
 					
 					
 				}//pitem
 				
 				$cart->save();
 				$this->_getSession()->setCartWasUpdated(true);
 				
 				/**
 				 * @todo remove wishlist observer processAddToCart
 				*/
 				Mage::dispatchEvent('checkout_cart_add_product_complete',
 				array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
 				);
 			
 				if (!$this->_getSession()->getNoCartRedirect(true)) {
 					if (!$cart->getQuote()->getHasError()){
 						$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
 						$response['status'] = 'SUCCESS';
 						$response['message'] = $message;
 					}
 				}
 			} catch (Mage_Core_Exception $e) {
 				$msg = "";
 				if ($this->_getSession()->getUseNotice(true)) {
 					$msg = $e->getMessage();
 				} else {
 					$messages = array_unique(explode("\n", $e->getMessage()));
 					foreach ($messages as $message) {
 						$msg .= $message.'<br/>';
 					}
 				}
 			
 				$response['status'] = 'ERROR';
 				$response['message'] = $msg;
 			} catch (Exception $e) {
 				$response['status'] = 'ERROR';
 				$response['message'] = $this->__('Cannot add the item to shopping cart.');
 				Mage::logException($e);
 			}
 			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
 			return;
 			
 			return;
 			
 		}else{
 			

 			
 			return parent::addAction();
 		}
 	}	
	
	
	
}