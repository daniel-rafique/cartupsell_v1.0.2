<?php
require_once 'Mage/Checkout/controllers/OnepageController.php';
class Thaneuk_Cartupsell_OnepageController extends  Mage_Checkout_OnepageController {

public function _getProduct($added,$pid){
static $prods;

	if(!isset($prods)){
	$arr= Mage::getStoreConfig('thaneuk_cartupsell_config/settings/products');
	
	$prods=array();
		$arr=explode("\n",$arr);
		foreach($arr as $ar){
		
			list($p,$l,$r)=explode(",",$ar);
			$prods[trim($p)]=array(trim($l),trim($r));
		
		}
		
	}
	if(!$pid){

		foreach ($prods as $pid=>$prod){
		break;
		}
	
	
	}
	if(!array_key_exists($pid, $prods)){
		return null;
	}
	reset($prods);
	$aItems = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
	$al=0;
	foreach($aItems as $item){

   if($pid==$item->getProductId()){
   	$al=true;
   	break;
   }
	
	}
		$pr=$prods[$pid];

		if($added||$al){
				if((int)$pr[0])
			return		Mage::getModel('catalog/product')->load($pr[0]);
		}else{
		if((int)$pr[1])
		return		Mage::getModel('catalog/product')->load($pr[1]);
		}
return null;		
	
}	
	
public function saveCartupsellAction() 
  {/*
    if ($this->_expireAjax()) {
     // return;
    }
    */
    try {
      if (!$this->getRequest()->isPost()) {
        $this->_ajaxRedirectResponse();
        return;
      }

     $added=$this->getRequest()->get('bCartUpselladdProductToCard');
     $pid=$this->getRequest()->get('prod_id');
      
    $sCartUpsellHtml = $this->_getCartupsellHtml($added==1,$pid);

    
    $result = array();
      
      	//|| count($aShownUpsells) > Mage::getStoreConfig('thaneuk_cartupsell_config/settings/upsell_count') 
      	if(empty($sCartUpsellHtml)||is_null($sCartUpsellHtml))
      	{
      		Zend_Json::$useBuiltinEncoderDecoder = false;
      		
      		if ($this->getOnepage()->getQuote()->isVirtual()) {
      			Mage::log("showing payment method");
      			 
      			$result['goto_section'] = 'payment';
      			$result['update_section'] = array(
      					'name' => 'payment-method',
      					'html' => $this->_getPaymentMethodsHtml()
      			);
      		}
      		else{
			   		
      		
      		Mage::log("shiploaded");
      		
      		$result['goto_section'] = 'shipping_method';
      		$result['update_section'] = array(
      				'name' => 'shipping-method',
      				'html' => $this->_getShippingMethodsHtml()
      		);}
      		
      	}
      	else
      	{
      		
      		Zend_Json::$useBuiltinEncoderDecoder = true;
      		$result['goto_section'] = 'cartupsell';
      		$result['update_section'] = array(
      				'name' => 'cartupsell',
      				'html' => $sCartUpsellHtml
      		);
      	}
      	
      	
      	
      	$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
      
    }catch(Mage_Exception $e){
    	
      	$this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('error'=>$e->getMessage())));
    }
  }
  
  /**
     * save checkout billing address
     */
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
//            $postData = $this->getRequest()->getPost('billing', array());
//            $data = $this->_filterPostData($postData);
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);
            $oSession = Mage::getSingleton('core/session');
            
///            $aCartUpsells = $this->_getCartUpsellProducts();
            $sCartUpsellHtml =trim( $this->_getCartupsellHtml());
            
            ///            Mage::log("billingUPsells".print_r($aCartUpsells,1));
            if (!isset($result['error'])) {
              if( is_null($sCartUpsellHtml)||empty($sCartUpsellHtml))
              {
              	if ($this->getOnepage()->getQuote()->isVirtual()) {
              		$result['goto_section'] = 'payment';
              		$result['update_section'] = array(
              				'name' => 'payment-method',
              				'html' => $this->_getPaymentMethodsHtml()
              		); 
              	} else{
              		if (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
              		$result['goto_section'] = 'cartupsell';
              		$result['update_section'] = array(
              				'name' => 'cartupsell',
              				'html' => $sCartUpsellHtml
              		);
              	
              		$result['allow_sections'] = array('shipping');
              		$result['duplicateBillingInfo'] = 'true';
              	} else {
              		$result['goto_section'] = 'shipping';
              	}
              }
              	
             
              }
              else 
              {
                ///$oSession->setShownUpsells(array());
               // $oSession->setCartUpsellNoFurtherProducts(false);
                //$oSession->setCartUpsells(null);
			    $result['goto_section'] = 'cartupsell';
                $result['update_section'] = array(
                            'name' => 'cartupsell',
                            'html' => $sCartUpsellHtml
                );
              
           
              }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
  
  /**
     * Shipping address save action
     */
    public function saveShippingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);
            
            $oSession = Mage::getSingleton('core/session');
            $oSession2 = Mage::getSingleton('checkout/session');
            
//            $aCartUpsells = $this->_getCartUpsellProducts();
            $sCartUpsellHtml = $this->_getCartupsellHtml();
            
            if( (empty($sCartUpsellHtml)||is_null($sCartUpsellHtml)))
              {
//               $this->loadLayout('checkout_onepage_review');
               $result['goto_section'] = 'shipping_method';
               $result['update_section'] = array(
                'name' => 'shipping-method',
                'html' => $this->_getShippingMethodsHtml()
               );
              }
              else 
              {
                $result['goto_section'] = 'cartupsell';
                $result['update_section'] = array(
                            'name' => 'cartupsell',
                            'html' => $sCartUpsellHtml
                );
                if (!isset($result['error'])) {
                    $result['goto_section'] = 'cartupsell';
                    $result['update_section'] = array(
                        'name' => 'cartupsell',
                        'html' => $sCartUpsellHtml
                    );
                }
              }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    protected function _getCartupsellHtml($added=0,$pid=0)
    {
    	$oLayout = $this->getLayout();
    	$oUpdate = $oLayout->getUpdate();
    	$cookie = Mage::getSingleton('core/cookie');
    
    
    	$product=$this->_getProduct($added, $pid);
    
    
    
    	if(!$product){
    		Mage::log("no cartx");
    
    		return null;
    	}
    
    	$cookie->set('currentupsell',$product->getId());
    
    
    
    	Mage::register('product', $product);
    	Mage::register('current_product',$product);
    
    	$productId  = $product->getId();
    
    	$design = Mage::getSingleton('catalog/design');
    	$settings = $design->getDesignSettings($product);
    
    	if ($settings->getCustomDesign()) {
    		$design->applyCustomDesign($settings->getCustomDesign());
    	}
    
    	$update = $this->getLayout()->getUpdate();
    	$update->addHandle('default');
    	$update->addHandle('checkout_onepage_cartupselloptions');
    	///   $update->addHandle('checkout_onepage_step');
    
    	// $controller->addActionLayoutHandles();
    
    	$update->addHandle('PRODUCT_TYPE_' . $product->getTypeId());
    	$update->addHandle('PRODUCT_' . $product->getId());
    	$this->loadLayoutUpdates();
    
    	// Apply custom layout update once layout is loaded
    	$layoutUpdates = $settings->getLayoutUpdates();
    	if ($layoutUpdates) {
    		if (is_array($layoutUpdates)) {
    			foreach($layoutUpdates as $layoutUpdate) {
    				$update->addUpdate($layoutUpdate);
    			}
    		}
    	}
    
    	//$this->generateLayoutXml()->generateLayoutBlocks();
    	$this->getLayout()->generateXml()->generateBlocks();
    	// Apply custom layout (page) template once the blocks are generated
    	if ($settings->getPageLayout()) {
    		$this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
    	}
    	return $this->getLayout()->getOutput();
    
    
    }
  protected function _getCartupsellHtml2($bCartUpselladdProductToCard=0,$added=0)
  {   
    $oLayout = $this->getLayout();
    $oUpdate = $oLayout->getUpdate();
    $cookie = Mage::getSingleton('core/cookie');
    
    $osession=Mage::getSingleton("checkout/session");
    $displayed= $cookie->get('currentupsell');//   $osession->getCartDisplayed();
    $showns= $cookie->get('shownupsells');
    if($showns)
    	$showns=unserialize($showns);
    else
    	$showns=array();   
    
    if($bCartUpselladdProductToCard&&$displayed){
	if(!in_array($displayed,$showns)){
    	$showns[]=$displayed;
		$cookie->set('shownupsells',serialize($showns));
	}
    }
    $product=$this->_getCartUpsell($showns,$added);
  
    Mage::log("showns ".print_r($showns,1));
    
    if(!$product){
    	Mage::log("no cartx");
    
    	return null;
    }

    $cookie->set('currentupsell',$product->getId());
   
    Mage::register('product', $product);
    Mage::register('current_product',$product);

    $productId  = $product->getId();

    $design = Mage::getSingleton('catalog/design');
    $settings = $design->getDesignSettings($product);
    
    if ($settings->getCustomDesign()) {
    	$design->applyCustomDesign($settings->getCustomDesign());
    }
    
     $update = $this->getLayout()->getUpdate();
     $update->addHandle('default');
     $update->addHandle('checkout_onepage_cartupselloptions');
///  $update->addHandle('checkout_onepage_step');
    
  // $controller->addActionLayoutHandles();
    
    $update->addHandle('PRODUCT_TYPE_' . $product->getTypeId());
    $update->addHandle('PRODUCT_' . $product->getId());
    $this->loadLayoutUpdates();
    
    // Apply custom layout update once layout is loaded
    $layoutUpdates = $settings->getLayoutUpdates();
    if ($layoutUpdates) {
    	if (is_array($layoutUpdates)) {
    		foreach($layoutUpdates as $layoutUpdate) {
    			$update->addUpdate($layoutUpdate);
    		}
    	}
    }
    
    //$this->generateLayoutXml()->generateLayoutBlocks();
    $this->getLayout()->generateXml()->generateBlocks();
    // Apply custom layout (page) template once the blocks are generated
    if ($settings->getPageLayout()) {
    	$this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
    }
    return $this->getLayout()->getOutput();
    

  }
  
  private function _getCartRelated($rserial=0){
  	$cookie = Mage::getSingleton('core/cookie');
  	$removed=$cookie->get('relateadded');
  	if($removed){
Mage::log("relatedadded");
///  		return array();
  	}  	
  	
  	
  	$oSession = Mage::getSingleton('checkout/session');
  	$aItems = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
  		
  	
  	$aCrosssellProducts = array();
  	$iStoreId = Mage::app()->getStore()->getId();
  	 
  	$iCrosssellProductCount = 0;
  	$aCrosssellProducts = array();
  	$items=array();
  	
  	$removed=$cookie->get('removeditem');
  	 Mage::log("removed ".$removed);
  	$c=0;
  	/*
  	if($removed){
  		$oProduct = Mage::getModel('catalog/product')->load($removed);
  		$aCrosssell = $oProduct->getRelatedProductCollection()->setOrder('sortorder', Varien_Db_Select::SQL_ASC);
  		Mage::log("related xsell cnt ".count($aCrosssell));
  		foreach($aCrosssell as $aCrosssellProduct)
  		{
  				
  			 
  			{
  				if(in_array($aCrosssellProduct["entity_id"],$items)){
  					return array();
  				}
  		
  					
  				 
  				$oUpsell = Mage::getModel('catalog/product')->load($aCrosssellProduct["entity_id"]);
  				//	if($oUpsell->isSalable())
  				{
  					$iCrosssellProductCount++;
  					$aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
  				}
  			}
  		
  		
  		}
  		
  		return $aCrosssellProducts;
  		
  	}
  	*/
  	foreach($aItems as $oItem)
  	{
  		$items[]=$oItem->getProductId();
  		if($oItem->getParentItemId()){
  			continue;
  		}
  		$oProduct = Mage::getModel('catalog/product')->load($oItem->getProductId());
  		if($oProduct->getTypeId()!=Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
  		$c++;
  		}
  	}
  	if($c>1){
  		Mage::log("count $c");
  		
  		///return array();
  	}
    	foreach($aItems as $oItem)
  	{
  		if($oItem->getParentItemId()){
  			continue;
  		}
  		$oProduct = Mage::getModel('catalog/product')->load($oItem->getProductId());
  		if($oProduct->getTypeId()==Mage_Catalog_Model_Product_Type::TYPE_BUNDLE){
  		continue;
  		}
  		
  		$aCrosssell = $oProduct->getRelatedProductCollection()->setOrder('sortorder', Varien_Db_Select::SQL_ASC);
  		 Mage::log("related xsell cnt ".count($aCrosssell));
  		foreach($aCrosssell as $aCrosssellProduct)
  		{
  			
  	
  			{
  				if(in_array($aCrosssellProduct["entity_id"],$items)){
  					continue;
  				}
  				
  				 
  	
  				$oUpsell = Mage::getModel('catalog/product')->load($aCrosssellProduct["entity_id"]);
  			//	if($oUpsell->isSalable())
  				{
  					$iCrosssellProductCount++;
  					$oUpsell->setSortorder($aCrosssellProduct['sortorder']);
  					$aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
  				}
  			}
  			 

  		}
  
  	}
  	
  	usort($aCrosssellProducts, function($a,$b){
  		if($a->getSortorder()==$b->getSortorder()){
  			return 0;
  		}
  		return  		$a->getSortorder()<$b->getSortorder() ?-1:1;
  	
  	});
  	 return $aCrosssellProducts;
  	
  }
  
  private function _getCartUpsells($serial=0){
  	$cookie = Mage::getSingleton('core/cookie');
  	$removed=$cookie->get('bundleadded');
 // 	if($removed)
  	//	return array();
  	
  	$oSession = Mage::getSingleton('checkout/session');
  	$aItems = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
  	$aCrosssellProducts = array();
  	$iStoreId = Mage::app()->getStore()->getId();
  	$iCrosssellProductCount=0;
$items=array();
  	foreach($aItems as $oItem)
  	{
  		$items[]=$oItem->getProductId();
  	}
  	///if(count($items)>1)
  		///return array();
  	foreach($aItems as $oItem)
  	{
  		$oProduct = Mage::getModel('catalog/product')->load($oItem->getProductId());
  		$aCrosssell = $oProduct->getUpsellProductCollection();
  		foreach($aCrosssell as $aCrosssellProduct)
  		{
  			
  			 
  			
  				
  				if(in_array($aCrosssellProduct["entity_id"],$items)){
  				continue;
  					//	return array();
  				}
  			
  				$oUpsell = Mage::getModel('catalog/product')->load($aCrosssellProduct["entity_id"]);
////  				echo "upsell ".$aCrosssellProduct['sortorder']."\n";
  				// if($oUpsell->isSalable())
  				{
  					$iCrosssellProductCount++;
  					$aCrosssellProducts[$iCrosssellProductCount] = &$oUpsell;
					$oUpsell->setSortorder($aCrosssellProduct['sortorder']);
  					$sorts[$iCrosssellProductCount]=$aCrosssellProduct['sortorder'];
  				}
  			  	
  		}
  		
  		
  	}
  	
  	usort($aCrosssellProducts, function($a,$b){
  		if($a->getSortorder()==$b->getSortorder()){
  			return 0;
  		}
return  		$a->getSortorder()<$b->getSortorder() ?-1:1;
  		
  	});
  	
  	return $aCrosssellProducts;
  }
  private function _getCartUpsellProducts(){
  	
  	$oSession = Mage::getSingleton('checkout/session');
  	$aItems = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
  	$aCrosssellProducts = array();
  	$iStoreId = Mage::app()->getStore()->getId();
  	//Mage::log("Store:".$iStoreId);
  	
  	/// $iCrosssellProductCount = 0;
  	///$oSession->setCurrentrelated(0);
  	///    $oSession->setCurrentupsell(0);
  	
  	foreach($aItems as $oItem)
  	{
  		$oProduct = Mage::getModel('catalog/product')->load($oItem->getProductId());
  		$aCrosssell = $oProduct->getUpsellProductCollection()->setOrder('sortorder', Varien_Db_Select::SQL_ASC);
  	
  		foreach($aCrosssell as $aCrosssellProduct)
  		{
  			 
  			{
  	
  				$oUpsell = Mage::getModel('catalog/product')->load($aCrosssellProduct["entity_id"]);
  				Mage::log("getCartUpsellProductsPossallab#".$oUpsell->isSalable());
  				{
  					$iCrosssellProductCount++;
  					$aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
  				}
  			}
  	
  		}
  	}
  	
  	$aCrosssellProducts2=$aCrosssellProducts;
  	$aCrosssellProducts = array();
  	$iStoreId = Mage::app()->getStore()->getId();
  	
  	$iCrosssellProductCount = 0;
  	$aCrosssellProducts = array();
  	
  	
  	foreach($aItems as $oItem)
  	{
  		$oProduct = Mage::getModel('catalog/product')->load($oItem->getId());
  	
  		$aCrosssell = $oProduct->getRelatedProductCollection()->setOrder('sortorder', Varien_Db_Select::SQL_ASC);
  	
  		foreach($aCrosssell as $aCrosssellProduct)
  		{
  			
  			 
  			
  	
  				$oUpsell = Mage::getModel('catalog/product')->load($aCrosssellProduct["entity_id"]);
  				//          $aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
  				if($oUpsell->isSalable())
  				{
  					$iCrosssellProductCount++;
  					$aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
  				}
  			
  	
  		}
  	}
  	
  	
  	return     array_merge($aCrosssellProducts2,$aCrosssellProducts);
  	
  	
  	
  }
  
  private  function _getCartUpsell($showns,$added=0)
  {
  	 
  	$aItems = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
  	$skus=array();
  	foreach($aItems as $item){
  	$skus[]=$item->getProductId();
  
  	}
  	
  	
  	$cu=0;
  	Mage::log("getupsellSKUS".print_r($skus,1));
  
  	$oSession=Mage::getSingleton("checkout/session");
  	{
  		$upsells=(array)$this->_getCartupsells();
  		Mage::log("upsell count ".count($upsells));

  		foreach($upsells as $upsell){
  			if(!in_array($upsell->getId(),$skus)&&!in_array($upsell->getId(),$showns)){
////  				$oSession->setData('currentupsell',$upsell);
  				Mage::log("current upsell set to ".$upsell->getId());
  				return $upsell;
  
  			}else if(in_array($upsell->getId(),$showns)){
  				Mage::log("upsell shown ".$upsell->getId());
  				if($added!=1)
  					break;
  				continue;
  			}
  			else{
  				Mage::log("upsell present ".$upsell->getId());
  				///$oSession->setData('currentupsell',$upsell->getProductId());
  continue;
  					//break;
  					
  			}
  		}
  
  
  
  
  
  		$upsells=	(array)$this->_getCartRelated();
  
  		Mage::log("related count ".count($upsells));
  		
  		foreach((array)$upsells as $upsell){
  			if(!in_array($upsell->getId(),$skus)&&!in_array($upsell->getId(),$showns)){
  				///$oSession->setCheckoutCurrentupsell($upsell->getId());
  				return $upsell;
  
  			}else if(in_array($upsell->getId(),$showns)){
  				if($added!=1)
  					break;
  				Mage::log("already shown ".$upsell->getId());
  				continue;
  			}
  			else{
  					Mage::log("relarted present ".$upsell->getId());
						continue;
  					///	$oSession->setCurrentrelated($upsell->getId());
  						//break;
  			}
  		}
  
  		return null;
  
  	}
  }
}
