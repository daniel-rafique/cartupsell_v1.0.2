<?php
class Thaneuk_Cartupsell_Block_Adminhtml_Related extends  Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Related{

	/**
	 * Add columns to grid
	 *
	 * @return Mage_Adminhtml_Block_Widget_Grid
	 */
	protected function _prepareColumns()
	{
		parent::_prepareColumns();
		$this->addColumn('sortorder', array(
				'header'    => Mage::helper('catalog')->__('Sortorder'),
				'index'     => 'sortorder',
				'width'=>10,
				'editable'=>true
		));
	
	}

	/**
	 * Retrieve upsell products
	 *
	 * @return array
	*/
	public function getSelectedRelatedProducts()
	{
		$products = array();
		foreach (Mage::registry('current_product')->getRelatedProducts() as $product) {
			$products[$product->getId()] = array('position' => $product->getPosition(),'sortorder' => $product->getSortorder());
		}
		return $products;
	}

}