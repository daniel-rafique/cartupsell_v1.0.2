<?php
class Thaneuk_Cartupsell_Block_Adminhtml_Upsell extends  Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Upsell{
	
	/**
	 * Add columns to grid
	 *
	 * @return Mage_Adminhtml_Block_Widget_Grid
	 */
	protected function _prepareColumns()
	{
		parent::_prepareColumns();
		$this->addColumn('sortorder', array(
				'header'    => Mage::helper('catalog')->__('Sortorder'),
				'index'     => 'sortorder',
				'width'=>10,
				'editable'=>true,
				'name'=>'sortorder',
				'type'=>'number',
				
		));
		
	}
	
	/**
	 * Retrieve upsell products
	 *
	 * @return array
	 */
	public function getSelectedUpsellProducts()
	{
		$products = array();
		foreach (Mage::registry('current_product')->getUpSellProducts() as $product) {
			$products[$product->getId()] = array('position' => $product->getPosition(),'sortorder' => $product->getSortorder());
		}
		return $products;
	}
	
}