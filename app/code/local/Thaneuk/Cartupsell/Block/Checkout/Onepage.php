<?php
class Thaneuk_Cartupsell_Block_Checkout_Onepage extends Mage_Checkout_Block_Onepage
{

  public function getSteps()
  {
    $aSteps = parent::getSteps(); // get steps from parent class
    
    foreach($aSteps as $sStepCode => $aStep) 
    {
      $aNewSteps[$sStepCode] = $aStep;
      if($sStepCode == "shipping")
        $aNewSteps['cartupsell'] = $this->getCheckout()->getStepData('cartupsell');
    }
    Mage::log($aNewSteps);
    return $aNewSteps;
    
  }

}