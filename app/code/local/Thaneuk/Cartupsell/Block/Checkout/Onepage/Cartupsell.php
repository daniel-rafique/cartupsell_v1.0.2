<?php
class Thaneuk_Cartupsell_Block_Checkout_Onepage_Cartupsell extends Mage_Checkout_Block_Onepage_Abstract
{
  protected function _construct()
  {
    $this->getCheckout()->setStepData('cartupsell', array(
            'label'     => $this->__('Would you like to upgrade your order?'),
            'is_show'   => $this->isShow()
    ));
    parent::_construct();
  }
  
}