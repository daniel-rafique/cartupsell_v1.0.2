<?php
class Thaneuk_Cartupsell_Model_Checkout_Type_Onepage extends Mage_Checkout_Model_Type_Onepage
{
  const REGISTRY_VAR_UPSELLS = 'thaneuk_cartupsell_upsells';
  const REGISTRY_VAR_SHOWN_UPSELLS = 'thaneuk_cartupsell_shown_upsells';
  
  const MAX_UPSELL_COUNT = 60;
  
  public static function getCartUpsellProducts()
  {//Mage::getStoreConfig('thaneaustria_cartupsell_config/settings/upsell_count')
    $oSession = Mage::getSingleton('checkout/session');
    $aItems = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
    $aCrosssellProducts = array();
    $iStoreId = Mage::app()->getStore()->getId();
    //Mage::log("Store:".$iStoreId);
    
   /// $iCrosssellProductCount = 0;
    ///$oSession->setCurrentrelated(0);
///    $oSession->setCurrentupsell(0);
    
    foreach($aItems as $oItem)
    { 
      $oProduct = Mage::getModel('catalog/product')->load($oItem->getProductId());
		Mage::log("loaded product ".$oProduct->getId());
      $aCrosssell = $oProduct->getUpsellProductCollection()->setOrder('sortorder', Varien_Db_Select::SQL_ASC);
      
      foreach($aCrosssell as $aCrosssellProduct)
      {
      	/*
        if(1 || $aCrosssellProduct["is_salable"] 
            && $aCrosssellProduct["type_id"] == "simple" 
            && $aCrosssellProduct["stock_item"]->getIsInStock()
          )*/
        	
        {
          
          Mage::log("getCartUpsellProductsPos#".$aCrosssellProduct["position"]."Art#".$aCrosssellProduct["sku"]);
          Mage::log("Upsell#".$iCrosssellProductCount);
          //$oUpsell = Mage::getModel('catalog/product')->setStoreId($iStoreId)->load($aCrosssellProduct["entity_id"]);
          $oUpsell = Mage::getModel('catalog/product')->load($aCrosssellProduct["entity_id"]);
//          $aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
        // if($oUpsell->isSalable())
          Mage::log("getCartUpsellProductsPossallab#".$oUpsell->isSalable());
          {
            $iCrosssellProductCount++;
            $aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
          }
        }
      
//        if($iCrosssellProductCount > (Mage::getStoreConfig('thaneaustria_cartupsell_config/settings/upsell_count')+1))
  //        break;
      }
    //  if($iCrosssellProductCount > (Mage::getStoreConfig('thaneaustria_cartupsell_config/settings/upsell_count')+1))
    
      //    break;
    }
    
    $oSession->setCartupsells($aCrosssellProducts);
    Mage::log("cartupsells set ".count($aCrosssellProducts));
	  $aCrosssellProducts2=$aCrosssellProducts;
    $aCrosssellProducts = array();
    $iStoreId = Mage::app()->getStore()->getId();
    //Mage::log("Store:".$iStoreId);
    
    $iCrosssellProductCount = 0;
    $aCrosssellProducts = array();
    
    
    foreach($aItems as $oItem)
    {
    	$oProduct = Mage::getModel('catalog/product')->load($oItem->getId());

    	$aCrosssell = $oProduct->getRelatedProductCollection()->setOrder('sortorder', Varien_Db_Select::SQL_ASC);
    
    	foreach($aCrosssell as $aCrosssellProduct)
    	{
    		/*
    		 if(1 || $aCrosssellProduct["is_salable"]
    		 		&& $aCrosssellProduct["type_id"] == "simple"
    		 		&& $aCrosssellProduct["stock_item"]->getIsInStock()
    		 )*/
    		 
    		{
    
    			Mage::log("Pos#".$aCrosssellProduct["position"]."Art#".$aCrosssellProduct["sku"]);
    			Mage::log("Upsell#".$iCrosssellProductCount);
    			//$oUpsell = Mage::getModel('catalog/product')->setStoreId($iStoreId)->load($aCrosssellProduct["entity_id"]);
    			$oUpsell = Mage::getModel('catalog/product')->load($aCrosssellProduct["entity_id"]);
    			//          $aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
    			if($oUpsell->isSalable())
    			{
    				$iCrosssellProductCount++;
    				$aCrosssellProducts[$iCrosssellProductCount] = $oUpsell;
    			}
    		}
    
    		//        if($iCrosssellProductCount > (Mage::getStoreConfig('thaneaustria_cartupsell_config/settings/upsell_count')+1))
    		//        break;
    	}
    	//  if($iCrosssellProductCount > (Mage::getStoreConfig('thaneaustria_cartupsell_config/settings/upsell_count')+1))
    
    	//    break;
    }
    
    $oSession->setCartRelated($aCrosssellProducts);

return     array_merge($aCrosssellProducts2,$aCrosssellProducts);
///    return $aCrosssellProducts;
  }
  
  public  function getCartUpsell($d=0)
  {
  	
  	
    $oSession = Mage::getSingleton('checkout/session');

    
    
    
    $cu=    $oSession->getCheckoutCurrentupsell();
$showns=(array)$oSession->getCheckoutShownupsells();

if($d){
	
	
	if(!$showns)
		$showns=array();
	
	$showns[]=$oSession->getCheckoutCurrentupsell();
if($cu){	
	$oSession->setCheckoutShownupsells($showns);
	Mage::log("added to list ".$cu);	 
}else{
	Mage::log("not added to list ".$cu);
	
}
}else{
	Mage::log("invoked from out");
	
}


$aItems = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
$skus=array();
foreach($aItems as $item){
	$skus[]=$item->getProductId();
	
}
$cu=0;
///Mage::log("getupsellSKUS".print_r($skus,1));

{
$upsells=(array)$oSession->getCartupsells();	
Mage::log("no cu count ".count($upsells));

		
		foreach($upsells as $upsell){
			if(!in_array($upsell->getId(),$skus)&&!in_array($upsell->getId(),$showns)){
				$oSession->setCheckoutCurrentupsell($upsell->getId());
				Mage::log("current upsell set to ".$upsell->getId());
				return $upsell;
				
			}else if(in_array($upsell->getId(),$skus)){
				break;
			}			
			else{
				///Mage::log("upsell present ".$upsell->getId());
				$oSession->setCheckoutCurrentupsell($upsell->getProductId());
				
			///	break;
			
			}
		}
		
$upsells=	(array)$oSession->getCartRelated();
	
foreach((array)$upsells as $upsell){
	if(!in_array($upsell->getId(),$skus)&&!in_array($upsell->getId(),$showns)){
		$oSession->setCheckoutCurrentupsell($upsell->getId());
		return $upsell;

	}else if(in_array($upsell->getId(),$skus)){
				break;
			}
	else{
	//	Mage::log("upsell present ".$upsell->getId());
	///	$oSession->setCurrentrelated($upsell->getId());
//		break;
	}
}
    
    return null;
 
}
}
}
  
